package com.example.demo.Controller;

import com.example.demo.Implement.CartAdd;
import com.example.demo.Implement.OrderImpl;
import com.example.demo.Model.Cart;
import com.example.demo.Model.Order;
import com.example.demo.Model.OrderAdd;
import com.example.demo.Model.Success;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OrderController {
    private OrderImpl orderImpl = new OrderImpl();

    @RequestMapping(value="/order/get/all", method = RequestMethod.GET)
    private List<Order> getorderAll(
    ){
        return orderImpl.GetOrderAll();
    };

    @RequestMapping(value="/order/customer/{customerId}/get", method = RequestMethod.GET)
    private List<Order> indexz(
            @PathVariable("customerId") String customerId
    ){
        return orderImpl.GetOrderCustomer(customerId);
    };

    @RequestMapping(value="/order/{orderId}/get", method = RequestMethod.GET)
    private Order getOrderById(
            @PathVariable("orderId") String orderId
    ){
        return orderImpl.GetOrderById(orderId);
    };

    @RequestMapping(value="/order/admin/status/get", method = RequestMethod.GET)
    private List<Order> getOrderByStatus(
            @RequestParam("status") String status
    ){
        return orderImpl.GetOrderAdmin(status);
    };

    @RequestMapping(value="/order/add", method = RequestMethod.POST)
    private Success addorder(
            @RequestBody OrderAdd orderAdd
    ){
        Success success = new Success();
        success = orderImpl.AddOrder(orderAdd);
        return success;
    };

    @RequestMapping(value="/order/validation/{orderId}", method = RequestMethod.POST)
    private Success validateOrder(
            @PathVariable("orderId") String orderId
    ){
        Success success = new Success();
        success = orderImpl.ValidateOrder(orderId);
        return success;
    };

    @RequestMapping(value="/order/{orderId}/shipping/{shippingNo}", method = RequestMethod.POST)
    private Success setShipping(
        @PathVariable("orderId") String orderId,
        @PathVariable("shippingNo") String shippingNo
    ){
        Success success = new Success();
        success = orderImpl.ShippingProcess(orderId,shippingNo);
        return success;
    };

}
