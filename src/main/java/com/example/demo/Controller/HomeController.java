package com.example.demo.Controller;

import com.example.demo.Model.Product;
import com.example.demo.Model.Success;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;

@RestController
public class HomeController {
    @RequestMapping(value="/", method = RequestMethod.GET)
    private Success indexz(){
        Success success = new Success();
        success.setCode("200");
        success.setMessage("Connect");
        return success;
    };
}
