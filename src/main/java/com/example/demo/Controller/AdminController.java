package com.example.demo.Controller;

import com.example.demo.Implement.AdminImpl;
import com.example.demo.Model.Admin;
import com.example.demo.Model.Success;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;

@RestController
public class AdminController {
    private AdminImpl adminImpl = new AdminImpl();

    @RequestMapping(value="/admin/get", method = RequestMethod.GET)
    public LinkedList<Admin> index(){
        return adminImpl.GetAdmin();
    };

    @RequestMapping(value="/admin/{adminId}/get", method = RequestMethod.GET)
    public Admin indexs(
            @PathVariable("adminId") String adminId
    ){

        return adminImpl.GetAdminOne(adminId);
    };

    @RequestMapping(value="/admin/add", method = RequestMethod.POST)
    public Success AddAdmin(
            @RequestBody Admin admin
    ){
        Success success = new Success();
        success = adminImpl.AddAdmin(admin);
        return success;
    };

    @RequestMapping(value="/admin/{adminId}/delete", method = RequestMethod.POST)
    public Success deleteAdmin(
            @PathVariable("adminId") String adminId
    ){
        int i = 0;
        Success success = new Success();
        success = adminImpl.DeleteAdmin(adminId);
        return success;
    };
}
