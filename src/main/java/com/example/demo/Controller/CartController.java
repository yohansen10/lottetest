package com.example.demo.Controller;

import com.example.demo.Implement.CartAdd;
import com.example.demo.Implement.CartImpl;
import com.example.demo.Model.Cart;
import com.example.demo.Model.Product;
import com.example.demo.Model.Success;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;

@RestController
public class CartController {
    private CartImpl cartImpl = new CartImpl();

    @RequestMapping(value="/cart/{customerId}/get", method = RequestMethod.GET)
    private Cart indexz(
            @PathVariable("customerId") String customerId
    ){
        return cartImpl.GetChart(customerId);
    };

    @RequestMapping(value="/cart/add", method = RequestMethod.POST)
    private Success AddCart(
            @RequestBody CartAdd cartAdd
    ){
        Success success = new Success();
        success = cartImpl.AddCart(cartAdd);
        return success;
    };

    @RequestMapping(value="/cart/edit", method = RequestMethod.POST)
    private Success EditCart(
            @RequestBody CartAdd cartAdd
    ){
        Success success = new Success();
        success = cartImpl.EditCart(cartAdd);
        return success;
    };

    @RequestMapping(value="/cart/delete", method = RequestMethod.POST)
    private Success DeleteCart(
            @RequestBody CartAdd cartAdd
    ){
        Success success = new Success();
        success = cartImpl.DeleteCart(cartAdd);
        return success;
    };
}
