package com.example.demo.Controller;

import com.example.demo.Implement.CustomerImpl;
import com.example.demo.Model.*;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;

@RestController
public class CustomerController{
    private CustomerImpl customerImpl = new CustomerImpl();

    @RequestMapping(value="/customer/get", method = RequestMethod.GET)
    public LinkedList<Customer> index(){
        return customerImpl.GetCustomer();
    };

    @RequestMapping(value="/paymentType/get", method = RequestMethod.GET)
    public LinkedList<PaymentType> indexPayment(){
        return customerImpl.GetPaymentType();
    };

    @RequestMapping(value="/customer/add", method = RequestMethod.POST)
    public Success AddProduct(
            @RequestBody Customer customer
    ){
        Success success = new Success();
        success = customerImpl.AddCustomer(customer);
        return success;
    };

    @RequestMapping(value="/customer/{customerId}/delete", method = RequestMethod.POST)
    public Success deleteproduct(
            @PathVariable(name = "customerId") String customerId
    ){
        Success success = new Success();
        success = customerImpl.DeleteCustomer(customerId);
        return success;
    };
}
