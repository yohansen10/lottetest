package com.example.demo.Controller;

import com.example.demo.Implement.ProductImpl;
import com.example.demo.Model.Product;
import com.example.demo.Model.Success;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;

@RestController
public class ProductController{

    private ProductImpl productImpl =new ProductImpl();
    @RequestMapping(value="/product/get", method = RequestMethod.GET)
    private LinkedList<Product> indexz(){
        return productImpl.GetProduct();
    };

    @RequestMapping(value="/product/add", method = RequestMethod.POST)
    private Success AddProduct(
            @RequestBody Product product
    ){
        Success success = new Success();
        success = productImpl.AddProduct(product);
        return success;
    };

    @RequestMapping(value="/product/{productId}/delete", method = RequestMethod.POST)
    public Success deleteproduct(
            @PathVariable(name = "productId") String productId
    ){
        Success success = new Success();
        success = productImpl.DeleteProduct(productId);
        return success;
    };
}
