package com.example.demo.Controller;

import com.example.demo.Implement.AddressImpl;
import com.example.demo.Model.Address;
import com.example.demo.Model.Success;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AddressController {
    private AddressImpl addressImpl = new AddressImpl();

    @RequestMapping(value="/address/{customerId}/get", method = RequestMethod.GET)
    public List<Address> index(
            @PathVariable("customerId") String customerId
    ){
        return addressImpl.GetAddress(customerId);
    };

    @RequestMapping(value="/address/add", method = RequestMethod.POST)
    public Success AddAdmin(
            @RequestBody Address address
    ){
        Success success = new Success();
        success = addressImpl.AddAddress(address);
        return success;
    };

    @RequestMapping(value="/address/{addressId}/delete", method = RequestMethod.POST)
    public Success deleteAdmin(
            @PathVariable("addressId") String addressId
    ){
        int i = 0;
        Success success = new Success();
        success = addressImpl.DeleteAddress(addressId);
        return success;
    };
}
