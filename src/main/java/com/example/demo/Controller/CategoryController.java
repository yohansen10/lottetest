package com.example.demo.Controller;

import com.example.demo.Implement.CategoryImpl;
import com.example.demo.Model.Category;
import com.example.demo.Model.Success;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;

@RestController
public class CategoryController{
    private CategoryImpl categoryImpl = new CategoryImpl();

    @RequestMapping(value="/category/get", method = RequestMethod.GET)
    public LinkedList<Category> indexs(){
        return categoryImpl.GetCategory();
    };

    @RequestMapping(value="/category/add", method = RequestMethod.POST)
    public Success AddProduct(
            @RequestBody Category category
    ){
        Success success = new Success();
        success = categoryImpl.AddCategory(category);
        return success;
    };

    @RequestMapping(value="/category/{categoryId}/delete", method = RequestMethod.POST)
    public Success deleteproduct(
            @PathVariable("categoryId") String categoryId
    ){
        int i = 0;
        Success success = new Success();
        success = categoryImpl.DeleteCategory(categoryId);
        return success;
    };
}
