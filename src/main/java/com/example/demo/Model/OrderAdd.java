package com.example.demo.Model;

public class OrderAdd
{
    private String orderId;
    private String paymentType;
    private Integer paymentMount;
    private String addressId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public Integer getPaymentMount() {
        return paymentMount;
    }

    public void setPaymentMount(Integer paymentMount) {
        this.paymentMount = paymentMount;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }
}
