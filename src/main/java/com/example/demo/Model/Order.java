package com.example.demo.Model;

import java.util.List;

public class Order {
    private String orderId;
    private String customerId;
    private String customerName;
    private List<CartDetail> orderProduct;
    private Integer summary;
    private String paymentType;
    private Integer paymentMount;
    private Address address;
    private String status;
    private String shippingNumber;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public List<CartDetail> getOrderProduct() {
        return orderProduct;
    }

    public void setOrderProduct(List<CartDetail> orderProduct) {
        this.orderProduct = orderProduct;
    }

    public Integer getSummary() {
        return summary;
    }

    public void setSummary(Integer summary) {
        this.summary = summary;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public Integer getPaymentMount() {
        return paymentMount;
    }

    public void setPaymentMount(Integer paymentMount) {
        this.paymentMount = paymentMount;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getShippingNumber() {
        return shippingNumber;
    }

    public void setShippingNumber(String shippingNumber) {
        this.shippingNumber = shippingNumber;
    }
}
