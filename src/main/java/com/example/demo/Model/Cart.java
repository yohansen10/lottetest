package com.example.demo.Model;

import java.util.List;

public class Cart {
    private String cartId;
    private String customerId;
    private List<CartDetail> cartProduct;

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public List<CartDetail> getCartProduct() {
        return cartProduct;
    }

    public void setCartProduct(List<CartDetail> cartProduct) {
        this.cartProduct = cartProduct;
    }
}
