package com.example.demo.Model;

public class CartDetail {
    private String productId;
    private String productName;
    private Integer price;
    private Integer priceSummary;
    private int qty;

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getPriceSummary() {
        return priceSummary;
    }

    public void setPriceSummary(Integer priceSummary) {
        this.priceSummary = priceSummary;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
}
