package com.example.demo.Implement;

import com.example.demo.Model.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.LinkedList;
public interface MainModel {

    public LinkedList<Product> linkedlistProduct = new LinkedList<Product>();
    public LinkedList<Customer> customerLinkedList = new LinkedList<Customer>();
    public LinkedList<Category> categoryLinkedList = new LinkedList<Category>();
    public LinkedList<Admin> adminLinkedList = new LinkedList<>();
    public LinkedList<Address> addressLinkedList = new LinkedList<>();
    public LinkedList<PaymentType> paymentTypeLinkedList = new LinkedList<>();
    public LinkedList<Cart> cartLinkedList = new LinkedList<>();
    public LinkedList<Order> orderLinkedList = new LinkedList<>();

}
