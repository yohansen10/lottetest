package com.example.demo.Implement;

import com.example.demo.Model.*;
import com.example.demo.Utils.RandomString;

import java.util.LinkedList;

public class CustomerImpl implements MainModel {

    public LinkedList<Customer> GetCustomer(){
        return customerLinkedList;
    }

    public Customer GetCustomerById(String customerId) {
        Customer customer1 = new Customer();
        try{
            int i = 0;
            for(Customer customer : customerLinkedList){
                if(customer.getCustomerId().equals(customerId)){
                    customer1 = customer;
                }
            }
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        return customer1;
    }

    public LinkedList<PaymentType> GetPaymentType(){
        if(paymentTypeLinkedList.size() <= 0){
            PaymentType paymentType = new PaymentType();
            paymentType.setPaymentId("001");
            paymentType.setPaymentName("Transfer");
            PaymentType paymentType1 = new PaymentType();
            paymentType1.setPaymentId("002");
            paymentType1.setPaymentName("Credit Card");
            paymentTypeLinkedList.add(paymentType);
            paymentTypeLinkedList.add(paymentType1);
        }
        return paymentTypeLinkedList;
    }

    public Success AddCustomer(Customer customer){
        Success success = new Success();
        try{
            if(customer.getCustomerId() == null){
                String customerId = new RandomString(32).nextString();
                customer.setCustomerId(customerId);
                customerLinkedList.add(customer);
                success.setMessage("Customer has been create");
            }else{
                if(customerLinkedList.size() > 0){
                    int i = 0;
                    for(Customer customer1 : customerLinkedList){
                        if(customer1.getCustomerId().equals(customer.getCustomerId())){
                            customerLinkedList.set(i,customer);
                            success.setMessage("Customer has been Edit");
                        }
                        i++;
                    }
                }else{
                    success.setMessage("Data empty, please insert product");
                    success.setCode("909");
                }
            }
            success.setCode("200");
        }catch (Exception ex){
            success.setMessage(ex.getMessage());
            success.setCode("909");
        }
        return success;
    }

    public Success DeleteCustomer(String customerId){
        Success success = new Success();
        int i = 0;
        try{
            if(customerLinkedList.size() > 0){
                for(Customer customer : customerLinkedList){
                    if(customer.getCustomerId().equals(customerId)){
                        customerLinkedList.remove(i);
                        success.setCode("200");
                        success.setMessage("Customer has been delete");
                        return success;
                    }else{
                        success.setCode("909");
                        success.setMessage("Customer not found");
                    }
                    i++;
                }
            }else{
                success.setMessage("Data empty, please insert product");
                success.setCode("909");
            }
        }catch (Exception ex){
            success.setCode("909");
            success.setMessage(ex.getMessage());
        }
        return success;
    }
}
