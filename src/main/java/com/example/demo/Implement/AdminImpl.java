package com.example.demo.Implement;

import com.example.demo.Model.Address;
import com.example.demo.Model.Admin;
import com.example.demo.Model.Category;
import com.example.demo.Model.Success;
import com.example.demo.Utils.RandomString;

import java.util.LinkedList;

public class AdminImpl implements MainModel {
    public LinkedList<Admin> GetAdmin(){
        return adminLinkedList;
    }

    public Admin GetAdminOne(String adminId){
        Admin admin = new Admin();
        try{
            int i = 0;
            if(adminLinkedList.size() > 0) {
                for(Admin admin1 : adminLinkedList){
                    if(admin1.getAdminId().equals(adminId)){
                        admin = admin1;
                    }
                }
            }else{
                return null;
            }
        }catch (Exception ex){
            System.out.println(ex.getMessage());
            return null;
        }
        return admin;
    }

    public Success AddAdmin(Admin admin){
        Success success = new Success();
        try{
            if(admin.getAdminId() == null){
                String adminId = new RandomString(32).nextString();
                admin.setAdminId(adminId);
                adminLinkedList.add(admin);
                success.setMessage("Admin has been create");
            }else{
                if(adminLinkedList.size() > 0){
                    int i = 0;
                    for(Admin admin1 : adminLinkedList){
                        if(admin1.getAdminId().equals(admin.getAdminId())){
                            adminLinkedList.set(i,admin);
                            success.setMessage("Admin has been Edit");
                        }
                        i++;
                    }
                }else{
                    success.setMessage("Data empty, please insert Admin");
                    success.setCode("909");
                }

            }
            success.setCode("200");
        }catch (Exception ex){
            success.setMessage(ex.getMessage());
            success.setCode("909");
        }
        return success;
    }

    public Success DeleteAdmin(String adminId){
        Success success = new Success();
        int i = 0;
        try{
            if(adminLinkedList.size() > 0){
                for(Admin admin : adminLinkedList){
                    if(admin.getAdminId().equals(adminId)){
                        adminLinkedList.remove(i);
                        success.setCode("200");
                        success.setMessage("Admin has been delete");
                        return success;
                    }else{
                        success.setCode("909");
                        success.setMessage("Admin not found");
                    }
                    i++;
                }
            }else{
                success.setMessage("Data empty, please insert category");
                success.setCode("909");
            }
        }catch (Exception ex){
            success.setCode("909");
            success.setMessage(ex.getMessage());
        }
        return success;
    }
}
