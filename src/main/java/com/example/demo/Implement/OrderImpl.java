package com.example.demo.Implement;

import com.example.demo.Model.*;

import java.util.LinkedList;
import java.util.List;

public class OrderImpl implements MainModel{
    private CartImpl cartImpl = new CartImpl();
    private CustomerImpl customerImpl = new CustomerImpl();
    private ProductImpl produtImpl = new ProductImpl();
    private AddressImpl addressImpl = new AddressImpl();
    public List<Order> GetOrderCustomer(String customerId) {
        LinkedList<Order> orderList = new LinkedList<>();
        try{
            int i = 0;
            if(orderLinkedList.size() > 0) {
                for(Order order : orderLinkedList){
                    if(order.getCustomerId().equals(customerId)){
                        orderList.add(order);
                    }
                }
            }
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        return orderList;
    }

    public Order GetOrderById(String orderId) {
        Order order = new Order();
        try{
            int i = 0;
            if(orderLinkedList.size() > 0) {
                for(Order order1 : orderLinkedList){
                    if(order1.getOrderId().equals(orderId)){
                        order = order1;
                    }
                }
            }
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        return order;
    }

    public List<Order> GetOrderAll() {
        return orderLinkedList;
    }

    public List<Order> GetOrderAdmin(String status) {
        LinkedList<Order> orderList = new LinkedList<>();
        try{
            int i = 0;
            if(orderLinkedList.size() > 0) {
                for(Order order : orderLinkedList){
                    if(order.getStatus().equals(status)){
                        orderList.add(order);
                    }
                }
            }
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        return orderList;
    }

    public Success AddOrder(OrderAdd orderAdd){

        Success success = new Success();
        try{
            Order order = new Order();
            Cart cart = cartImpl.GetCartById(orderAdd.getOrderId());
            Customer customer = customerImpl.GetCustomerById(cart.getCustomerId());
            order.setOrderId(cart.getCartId());
            order.setCustomerId(cart.getCustomerId());
            order.setCustomerName(customer.getCustomerName());
            order.setStatus("Payment Checking");
            order.setOrderProduct(cart.getCartProduct());
            order.setPaymentMount(orderAdd.getPaymentMount());
            order.setPaymentType(orderAdd.getPaymentType());
            Address address = addressImpl.GetAddressOne(orderAdd.getAddressId());
            order.setAddress(address);
            order.setSummary(cartImpl.GetSummary(cart.getCartId()));
            orderLinkedList.add(order);
            produtImpl.UpdateStock((LinkedList<CartDetail>) order.getOrderProduct());
            success.setCode("200");
            success.setMessage("Data has been save");
        }catch (Exception ex){
            success.setCode("909");
            success.setMessage(ex.getMessage());
        }
        return success;
    }

    public Success ValidateOrder(String orderId){
        Success success = new Success();
        try{
            int k=0;
            for(Order order : orderLinkedList){
                if(order.getOrderId().equals(orderId)){
                    order.setStatus("On Process");
                    orderLinkedList.set(k,order);
                    success.setCode("200");
                    success.setMessage("Data has been save");
                }
                k++;
            }
        }catch (Exception ex){
            success.setCode("909");
            success.setMessage(ex.getMessage());
        }

        return success;
    }

    public Success ShippingProcess(String orderId, String shippingNo){
        Success success = new Success();
        try{
            int k=0;
            for(Order order : orderLinkedList){
                if(order.getOrderId().equals(orderId)){
                    order.setStatus("On Shipping");
                    order.setShippingNumber(shippingNo);
                    orderLinkedList.set(k,order);
                    success.setCode("200");
                    success.setMessage("Data has been save");
                }
                k++;
            }
        }catch (Exception ex){
            success.setCode("909");
            success.setMessage(ex.getMessage());
        }
        return success;
    }

}
