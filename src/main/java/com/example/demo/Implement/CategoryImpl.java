package com.example.demo.Implement;

import com.example.demo.Model.Category;
import com.example.demo.Model.Success;
import com.example.demo.Utils.RandomString;

import java.util.LinkedList;

public class CategoryImpl implements MainModel{
    public LinkedList<Category> GetCategory(){
        return categoryLinkedList;
    }

    public Success AddCategory(Category category){
        Success success = new Success();
        try{
            if(category.getCategoryId() == null){
                String productId = new RandomString(32).nextString();
                category.setCategoryId(productId);
                categoryLinkedList.add(category);
                success.setMessage("Category has been create");
            }else{
                if(categoryLinkedList.size() > 0){
                    int i = 0;
                    for(Category category1 : categoryLinkedList){
                        if(category1.getCategoryId().equals(category.getCategoryId())){
                            categoryLinkedList.set(i,category);
                            success.setMessage("Category has been Edit");
                        }
                        i++;
                    }
                }else{
                    success.setMessage("Data empty, please insert category");
                    success.setCode("909");
                }

            }
            success.setCode("200");
        }catch (Exception ex){
            success.setMessage(ex.getMessage());
            success.setCode("909");
        }
        return success;
    }

    public Success DeleteCategory(String categoryId){
        Success success = new Success();
        int i = 0;
        try{
            if(categoryLinkedList.size() > 0){
                for(Category category1 : categoryLinkedList){
                    if(category1.getCategoryId().equals(categoryId)){
                        categoryLinkedList.remove(i);
                        success.setCode("200");
                        success.setMessage("Category has been delete");
                        return success;
                    }else{
                        success.setCode("909");
                        success.setMessage("Category not found");
                    }
                    i++;
                }
            }else{
                success.setMessage("Data empty, please insert category");
                success.setCode("909");
            }
        }catch (Exception ex){
            success.setCode("909");
            success.setMessage(ex.getMessage());
        }
        return success;
    }
}
