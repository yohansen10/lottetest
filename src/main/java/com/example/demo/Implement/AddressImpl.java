package com.example.demo.Implement;

import com.example.demo.Model.Address;
import com.example.demo.Model.Success;
import com.example.demo.Utils.RandomString;

import java.util.LinkedList;
import java.util.List;

public class AddressImpl implements MainModel{

    public List<Address> GetAddress(String customerId){
        LinkedList<Address> address = new LinkedList<>();
        try{
            int i = 0;
            if(addressLinkedList.size() > 0) {
                for(Address address1 : addressLinkedList){
                    if(address1.getCustomerId().equals(customerId)){
                        address.add(address1);
                    }
                }
            }else{
                return null;
            }
        }catch (Exception ex){
            System.out.println(ex.getMessage());
            return null;
        }
        return address;
    }

    public Address GetAddressOne(String addressId){
        Address address = new Address();
        try{
            int i = 0;
            if(addressLinkedList.size() > 0) {
                for(Address address1 : addressLinkedList){
                    if(address1.getAddressId().equals(addressId)){
                        address = address1;
                    }
                }
            }
        }catch (Exception ex){
            System.out.println(ex.getMessage());
            return null;
        }
        return address;
    }

    public Success AddAddress(Address address){
        Success success = new Success();
        try{
            if(address.getAddressId() == null){
                String addressId = new RandomString(32).nextString();
                address.setAddressId(addressId);
                addressLinkedList.add(address);
                success.setMessage("Address has been create");
            }else{
                if(addressLinkedList.size() > 0){
                    int i = 0;
                    for(Address address1 : addressLinkedList){
                        if(address1.getAddressId().equals(address.getAddressId())){
                            addressLinkedList.set(i,address);
                            success.setMessage("Address has been Edit");
                        }
                        i++;
                    }
                }else{
                    success.setMessage("Data empty, please insert Address");
                    success.setCode("909");
                }
            }
            success.setCode("200");
        }catch (Exception ex){
            success.setMessage(ex.getMessage());
            success.setCode("909");
        }
        return success;
    }

    public Success DeleteAddress(String addressId){
        Success success = new Success();
        int i = 0;
        try{
            if(addressLinkedList.size() > 0){
                for(Address address : addressLinkedList){
                    if(address.getAddressId().equals(addressId)){
                        addressLinkedList.remove(i);
                        success.setCode("200");
                        success.setMessage("Address has been delete");
                        return success;
                    }else{
                        success.setCode("909");
                        success.setMessage("Address not found");
                    }
                    i++;
                }
            }else{
                success.setMessage("Data empty, please insert category");
                success.setCode("909");
            }
        }catch (Exception ex){
            success.setCode("909");
            success.setMessage(ex.getMessage());
        }
        return success;
    }
}
