package com.example.demo.Implement;

import com.example.demo.Model.Admin;
import com.example.demo.Model.CartDetail;
import com.example.demo.Model.Product;
import com.example.demo.Model.Success;
import com.example.demo.Utils.RandomString;

import java.util.LinkedList;

public class ProductImpl implements MainModel{
    public LinkedList<Product> GetProduct(){
        return linkedlistProduct;
    }
    public Product GetProductOne(String productId){
        Product product = new Product();
        try{
            int i = 0;
            if(linkedlistProduct.size() > 0) {
                for(Product product1 : linkedlistProduct){
                    if(product1.getProductId().equals(productId)){
                        product = product1;
                    }
                }
            }else{
                product = null;
            }
        }catch (Exception ex){
            System.out.println(ex.getMessage());
            return null;
        }
        return product;
    }
    public Success AddProduct(Product product){
        Success success = new Success();
        try{
            if(product.getProductId() == null){
                String productId = new RandomString(32).nextString();
                product.setProductId(productId);
                linkedlistProduct.add(product);
                success.setMessage("Product has been create");
            }else{
                if(linkedlistProduct.size() > 0){
                    int i = 0;
                    for(Product product1 : linkedlistProduct){
                        if(product1.getProductId().equals(product.getProductId())){
                            linkedlistProduct.set(i,product);
                            success.setMessage("Product has been Edit");
                        }
                        i++;
                    }
                }else{
                        success.setMessage("Data empty, please insert product");
                        success.setCode("909");
                }
            }
            success.setCode("200");
        }catch (Exception ex){
            success.setMessage(ex.getMessage());
            success.setCode("909");
        }
        return success;
    }

    public Product CekStock(String productId, Integer qty){
        //Integer resp = 0;
        Product product = new Product();
        product = this.GetProductOne(productId);
        if(product.getStock() >= qty){
            return product;
        }else{
            product = null;
        }
        return product;
    }

    public Success DeleteProduct(String productId){
        Success success = new Success();
        int i = 0;
        try{
            if(linkedlistProduct.size() > 0){
                for(Product product : linkedlistProduct){
                    if(product.getProductId().equals(productId)){
                        linkedlistProduct.remove(i);
                        success.setCode("200");
                        success.setMessage("Product has been delete");
                        return success;
                    }else{
                        success.setCode("909");
                        success.setMessage("Product not found");
                    }
                    i++;
                }
            }else{
                success.setMessage("Data empty, please insert product");
                success.setCode("909");
            }
        }catch (Exception ex){
            success.setCode("909");
            success.setMessage(ex.getMessage());
        }
        return success;
    }

    public void UpdateStokOne(String productId,Integer qty){
        int i = 0;
        for(Product product : linkedlistProduct){
            if(product.getProductId().equals(productId)){
                product.setStock(product.getStock() - qty);
                linkedlistProduct.set(i,product);
            }
            i++;
        }
    }

    public void UpdateStock(LinkedList<CartDetail> cartDetailList){
        for(CartDetail cartDetail : cartDetailList){
            this.UpdateStokOne(cartDetail.getProductId(),cartDetail.getQty());
        }
    }
}
