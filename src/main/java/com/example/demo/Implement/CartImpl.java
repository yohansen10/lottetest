package com.example.demo.Implement;

import com.example.demo.Model.Cart;
import com.example.demo.Model.CartDetail;
import com.example.demo.Model.Product;
import com.example.demo.Model.Success;
import com.example.demo.Utils.RandomString;

import java.util.LinkedList;
import java.util.List;


public class CartImpl implements MainModel{
    private ProductImpl productImpl = new ProductImpl();

    public Cart GetChart(String customerId) {
        Cart cart = new Cart();
        //cart = null;
        try{
            int i = 0;
            if(cartLinkedList.size() > 0) {
                for(Cart cart1 : cartLinkedList){
                    if(cart1.getCustomerId().equals(customerId)){
                        cart = cart1;
                    }
                }
            }
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        return cart;
    }

    public Cart GetCartById(String carId) {
        Cart cart = new Cart();
        cart = null;
        try{
            int i = 0;
            if(cartLinkedList.size() > 0) {
                for(Cart cart1 : cartLinkedList){
                    if(cart1.getCartId().equals(carId)){
                        cart = cart1;
                    }
                }
            }
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        return cart;
    }

    public Integer GetSummary(String cartId){
        Integer summary = 0;
        for(Cart cart1 : cartLinkedList){
            if(cart1.getCartId().equals(cartId)){
                List<CartDetail> cartDetailList = cart1.getCartProduct();
                for (CartDetail cartDetail : cartDetailList){
                    summary += cartDetail.getPriceSummary();
                }
            }
        }
        return summary;
    }
    public Success AddCart(CartAdd cartAdd){
        Success success = new Success();
        try{
            Cart cart = new Cart();
            if(cartLinkedList.size() > 0){
               cart = this.GetChart(cartAdd.getCustomerId());
               if(cart.getCartId() == null){
                   String cartId = new RandomString(32).nextString();
                   Cart cart1 = new Cart();
                   CartDetail cartDetail = new CartDetail();
                   cart1.setCartId(cartId);
                   cart1.setCustomerId(cartAdd.getCustomerId());
                   Product product = productImpl.CekStock(cartAdd.getProductId(),cartAdd.getQty());
                   if(product == null){
                       success.setCode("909");
                       success.setMessage("Stock not available");
                   }else{
                       List<CartDetail> cartDetailList = null;
                       cartDetail.setProductId(cartAdd.getProductId());
                       cartDetail.setPrice(product.getPrice());
                       cartDetail.setProductName(product.getProductName());
                       cartDetail.setQty(cartAdd.getQty());
                       cartDetail.setPriceSummary(cartAdd.getQty() * product.getPrice());
                       cartDetailList.add(cartDetail);
                       cart1.setCartProduct(cartDetailList);
                       cartLinkedList.add(cart1);
                   }

               }else{
                   List<CartDetail> cartDetailList = cart.getCartProduct();
                   int j = 0;
                   Boolean isTrue = false;
                   for(CartDetail cartDetail : cartDetailList){
                       if(cartDetail.getProductId().equals(cartAdd.getProductId())){
                           cartDetail.setQty(cartDetail.getQty()+cartAdd.getQty());
                           cartDetail.setPriceSummary(cartDetail.getQty()*cartDetail.getPrice());
                           Product product = productImpl.CekStock(cartAdd.getProductId(),(cartDetail.getQty()+cartAdd.getQty()));
                           if(product.getProductId() == null){
                               success.setCode("909");
                               success.setMessage("Stock not available");
                               return success;
                           }else{
                               cartDetailList.set(j,cartDetail);
                               isTrue = true;
                           }
                       }
                       j++;
                   }
                   if(!isTrue){
                       Product product = productImpl.CekStock(cartAdd.getProductId(),cartAdd.getQty());
                       if(product == null){
                           success.setCode("909");
                           success.setMessage("Stock not available");
                           return success;
                       }else{
                           CartDetail cartDetail = new CartDetail();
                           cartDetail.setProductId(cartAdd.getProductId());
                           cartDetail.setPrice(product.getPrice());
                           cartDetail.setProductName(product.getProductName());
                           cartDetail.setQty(cartAdd.getQty());
                           cartDetail.setPriceSummary(cartAdd.getQty() * product.getPrice());
                           cartDetailList.add(cartDetail);
                       }
                   }
                   cart.setCartProduct(cartDetailList);
                   int l = 0;
                   for(Cart cart1 : cartLinkedList){
                       if(cart1.getCustomerId().equals(cartAdd.getCustomerId())){
                           cartLinkedList.set(l,cart);
                           success.setCode("200");
                           success.setMessage("Product has been add to cart");
                       }
                       l++;
                   }
               }
            }else{
                String cartId = new RandomString(32).nextString();
                Cart cart1 = new Cart();
                CartDetail cartDetail = new CartDetail();
                cart1.setCartId(cartId);
                cart1.setCustomerId(cartAdd.getCustomerId());
                Product product = productImpl.CekStock(cartAdd.getProductId(),cartAdd.getQty());
                if(product.getProductId() == null){
                    success.setCode("909");
                    success.setMessage("Stock not available");
                }else{
                    List<CartDetail> cartDetailList = new LinkedList<>();
                    cartDetail.setProductId(cartAdd.getProductId());
                    cartDetail.setPrice(product.getPrice());
                    cartDetail.setProductName(product.getProductName());
                    cartDetail.setQty(cartAdd.getQty());
                    cartDetail.setPriceSummary(cartAdd.getQty() * product.getPrice());
                    cartDetailList.add(cartDetail);
                    cart1.setCartProduct(cartDetailList);
                    cartLinkedList.add(cart1);
                    success.setCode("200");
                    success.setMessage("Product has been add to cart");
                }
            }
        }catch (Exception ex){
            success.setMessage(ex.getMessage());
            success.setCode("909");
        }
        return success;
    }

    public Success EditCart(CartAdd cartAdd){
        Success success = new Success();
        if(cartLinkedList.size() > 0){
            int i = 0;
            for(Cart cart1 : cartLinkedList){
                if(cart1.getCustomerId().equals(cartAdd.getCustomerId())){
                    List<CartDetail> cartDetailList = cart1.getCartProduct();
                    int j =0;
                    for (CartDetail cartDetail : cartDetailList){
                        if(cartDetail.getProductId().equals(cartAdd.getProductId())){
                            cartDetail.setQty(cartAdd.getQty());
                            cartDetail.setPriceSummary(cartDetail.getQty()*cartDetail.getPrice());
                            cartDetailList.set(j,cartDetail);
                        }
                        j++;
                    }
                    cart1.setCartProduct(cartDetailList);
                    cartLinkedList.set(i,cart1);
                    success.setCode("200");
                    success.setMessage("Product has been edit from cart");
                }
                i++;
            }
        }else{
            success.setCode("200");
            success.setMessage("Cart not available");
        }
        return success;
    }

    public Success DeleteCart(CartAdd cartAdd){
        Success success = new Success();
        if(cartLinkedList.size() > 0){
            int i = 0;
            for(Cart cart1 : cartLinkedList){
                if(cart1.getCustomerId().equals(cartAdd.getCustomerId())){
                    List<CartDetail> cartDetailList = cart1.getCartProduct();
                    int j =0;
                    for (CartDetail cartDetail : cartDetailList){
                        if(cartDetail.getProductId().equals(cartAdd.getProductId())){
                            cartDetailList.remove(j);
                        }
                        j++;
                    }
                    cart1.setCartProduct(cartDetailList);
                    cartLinkedList.set(i,cart1);
                    success.setCode("200");
                    success.setMessage("Product has been delete from cart");
                }
                i++;
            }
        }else{
            success.setCode("200");
            success.setMessage("Cart not available");
        }
        return success;
    }
}
